(ns figly.scanner-test
  (:require [cljs.test :refer-macros [deftest is]]
            [planck.core :refer [slurp with-open line-seq *reader-fn*]]
            [figly.scanner :as sut]
            [clojure.string :as string]))

(defn read-example-cases []
  (->>
    (with-open [r (*reader-fn* "test/example-cases.txt")]
      (doall (line-seq r)))
    (filter (complement #(string/starts-with? % ";")))
    (partition 5)
    (map (juxt #(take 4 %) #(last %)))
    (into {})))

(deftest example-cases
  (doseq [[account-example expected] (read-example-cases)]
    (is
      (= expected
        (sut/parse-account-scan account-example)))))

