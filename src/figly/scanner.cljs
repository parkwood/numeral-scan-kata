(ns figly.scanner
  (:require [cljs.spec :as s] 
            [planck.core :refer [with-open line-seq *reader-fn* *writer-fn*]]))

(def ^:private numeral->num
  {
"
 _ 
| |
|_|" 0

"
   
  |
  |" 1

"
 _ 
 _|
|_ " 2

"
 _ 
 _|
 _|" 3


"
   
|_|
  |" 4

"
 _ 
|_ 
 _|" 5

"
 _ 
|_ 
|_|" 6

"
 _ 
  |
  |" 7

"
 _ 
|_|
|_|" 8

"
 _ 
|_|
 _|" 9
   })

(s/fdef ->numerals
  :args (s/cat :account-scan
          (s/coll-of string?))
  :ret (s/coll-of string?))

(defn- ->numerals [numeral-lines]
  (let [non-blank-lines-count 3
        numeral-width 3]
    (->> numeral-lines
      (take non-blank-lines-count)
      (map (comp
             #(map (fn [x] (apply str x)) %)
             #(partition numeral-width %)))
      (apply map (fn [& lines]
                   (apply str (interleave (repeat "\n") lines)))))))

(s/fdef valid?
  :args (s/cat :account-digits
          (s/coll-of int?))
  :ret boolean?)

(defn- valid? [account-digits]
  (as->
    account-digits $
    (map
      *
      [9 8 7 6 5 4 3 2 1] $)
    (apply + $)
    (mod $ 11)
    (zero? $)))

(s/fdef parse-account-scan
  :args (s/cat :account-scan 
          (s/coll-of string?))
  :ret string?)

(defn parse-account-scan [account-scan]
  (let [found-numbers (->> account-scan
                        ->numerals
                        (map #(get numeral->num % "?")))
        ill-formed? (some #(= "?" %) found-numbers)]
    (str
      (apply str found-numbers)
      (if ill-formed?
        " ILL"
        (when-not (valid? found-numbers) " ERR")))))

(defn read-scan-file! [scan-file output-file]
  (let [lines-per-account 4]
    (->>
      (with-open [r (*reader-fn* scan-file)
                  w (*writer-fn* output-file)]
        (doseq [parse-output (->> (line-seq r)
                               (partition lines-per-account)
                               (map parse-account-scan))]
          (-write w (str parse-output "\n")))))))



